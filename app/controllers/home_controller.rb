class HomeController < ApplicationController

  def index
    @filterrific = initialize_filterrific(
      Product,
      params[:filterrific],
      :select_options => {
        sorted_by: Product.options_for_sorted_by,
        with_parent: MarketplaceCategory.options_for_select,
        with_child: MarketplaceCategory.options_for_select_children(params[:filterrific]),
        commision_amount_in: Shop.options_for_select_commision,
      }
    ) or return
    @products = @filterrific.find
    @child_category_id = params[:filterrific].present? ? params[:filterrific][:with_child] : nil

    respond_to do |format|
      format.html
      format.js
    end
  end
  
end