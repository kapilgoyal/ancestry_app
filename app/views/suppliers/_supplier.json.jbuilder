json.extract! supplier, :id, :name, :primary_category, :secondary_category, :created_at, :updated_at
json.url supplier_url(supplier, format: :json)
