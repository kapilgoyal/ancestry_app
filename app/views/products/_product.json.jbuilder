json.extract! product, :id, :name, :price, :marketplace_category_id, :shop_id, :created_at, :updated_at
json.url product_url(product, format: :json)
