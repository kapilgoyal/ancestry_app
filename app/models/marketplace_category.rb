class MarketplaceCategory < ApplicationRecord

	has_ancestry

	has_many :products

  def self.options_for_select
    roots.map{|mp| [mp.name, mp.id]}
  end

  def self.options_for_select_children(filterrific_params)
		(filterrific_params.present? && filterrific_params[:with_parent].present?) ? where(id: filterrific_params[:with_parent]).first.children.map{|mp| [mp.name, mp.id]} : []
  end

  def self.options_for_select_children_from_name(filterrific_params)
		(filterrific_params.present? && filterrific_params[:with_parent].present?) ? where(name: filterrific_params[:with_parent]).first.children.map{|mp| [mp.name, mp.name]} : []
  end

end