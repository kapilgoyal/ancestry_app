class Shop < ApplicationRecord

	has_many :products

	def self.options_for_select_commision
    [
      ['0-50', '0-50'],
      ['50-100', '50-100']
    ]
  end

  def self.options_for_select
    all.map{|mp| [mp.name, mp.id]}
  end

end