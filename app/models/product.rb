class Product < ActiveRecord::Base

  attr_accessor :primary_category_id

	belongs_to :marketplace_category, optional: true
  belongs_to :shop, optional: true

  validates_presence_of :name, :price, :primary_category_id, :marketplace_category_id, :shop_id

  filterrific :default_filter_params => { :sorted_by => 'created_at_desc' },
              :available_filters => %w[
                sorted_by
                search_query
                with_parent
                with_child
                commision_amount_in
              ]

  scope :search_query, lambda { |query|
    return nil  if query.blank?
    # condition query, parse into individual keywords
    terms = query.downcase.split(/\s+/)
    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      ('%' + e.gsub('*', '%') + '%').gsub(/%+/, '%')
    }
    # configure number of OR conditions for provision
    # of interpolation arguments. Adjust this if you
    # change the number of OR conditions.
    # num_or_conditions = 3
    num_or_conditions = 1
    where(
      terms.map {
        or_clauses = [
          # "LOWER(products.name) LIKE ?",
          # "LOWER(products.name) LIKE ?",
          "LOWER(products.name) LIKE ?"
        ].join(' OR ')
        "(#{ or_clauses })"
      }.join(' AND '),
      *terms.map { |e| [e] * num_or_conditions }.flatten
    )
  }

  scope :sorted_by, lambda { |sort_option|
    # extract the sort direction from the param value.
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at_/
      order("products.created_at #{ direction }")
    when /^updated_at_/
      order("products.updated_at #{ direction }")
    when /^name_/
      order("LOWER(products.name) #{ direction }, LOWER(products.name) #{ direction }")
    # when /^country_name_/
    #   order("LOWER(countries.name) #{ direction }").includes(:country)
    else
      raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
    end
  }

  scope :with_parent, lambda { |parent_ids|
  	child_ids = MarketplaceCategory.where(id: parent_ids).first.children.map(&:id)
    where(:marketplace_category_id => [*child_ids])
  }

  scope :with_child, lambda { |child_ids|
    where(:marketplace_category_id => [*child_ids])
  }

  scope :commision_amount_in, lambda { |limit|
    range = limit.split("-")
    shop_ids = Shop.where(commision: range[0].to_i..range[1].to_i).map(&:id)
    where(:shop_id => [*shop_ids])
  }

  def self.options_for_sorted_by
    [
      ['Created At', 'created_at_desc'],
      ['Updated At', 'updated_at_desc'],
      ['Name (A-Z)', 'name_asc'],
      ['Name (Z-A)', 'name_desc']
    ]
  end

end
