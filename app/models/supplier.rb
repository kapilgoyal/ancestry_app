class Supplier < ApplicationRecord

  validates_presence_of :name, :primary_category, :secondary_category

end