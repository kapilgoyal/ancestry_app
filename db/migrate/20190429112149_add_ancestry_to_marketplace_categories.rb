class AddAncestryToMarketplaceCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :marketplace_categories, :ancestry, :string
    add_index :marketplace_categories, :ancestry
  end
end
