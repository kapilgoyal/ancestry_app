class CreateSuppliers < ActiveRecord::Migration[5.1]
  def change
    create_table :suppliers do |t|
      t.string :name
      t.string :primary_category
      t.string :secondary_category

      t.timestamps
    end
  end
end
